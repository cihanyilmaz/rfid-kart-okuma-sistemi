#include <SPI.h>
#include <MFRC522.h>
#include <EEPROM.h>
#include <Servo.h>

// motor ko başla 
Servo servom;  // bir servo objesi yaratıyoruz
int poz = 0;    // servomuzun pozisyonunu belirliyoruz

// motor kod bitiş

//buzzer kod başlangıç

#define doo  1000// do komutu ile karismasin diye doo yazdim (kalin do)
#define re   1122
#define mi   1260
#define fa   1335
#define sol  1498
#define la   1681
#define si   1887
#define dom  2000// do komutu ile karismasin diye dom yazdim (ince do)

int melodi[] =     {doo,re,dom ,si,doo,si,doo,re,dom ,si,doo,si,doo,re,dom ,si,doo,si,  la,si,la,sol,fa,la };
int notasuresi[] = {6,  6,  2,  6, 6,  2,6,  6,  2, 6 , 6,  2,6,  6,  2,  6, 6,  2,  6,9,8,5,5,2 };// her notanin süresini array yaptim
int tempo = 1200;





int buzzerPin = 4;
int notaSayisi = 8;
int C = 262;
int D = 294;
int E = 330;
int F = 349;
int G = 392;
int A = 440;
int B = 494;
int C_ = 523;
int notalar[] = {C, D, E, F, G, A, B, C_};

//buzzer kod bitiş



 
#define RST_PIN 9
#define SS_PIN 10

byte readCard[4];
int successRead;
String tam="";
MFRC522 mfrc522(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key;

void setup()
{
  
   //motor kod başla
   servom.attach(5);  // yarattığımız servo objesinin bağlı olduğu pini yazıyoruz

   //motor kod bitiş
   
 pinMode(7, OUTPUT);
Serial.begin(9600);
  SPI.begin();
  mfrc522.PCD_Init();
 // Serial.println("RFID KART KAYIT UYGULAMASI");
 
  while(true)
  {
    
  //Serial.print("Lutfen karti okutun");
  
  do {
    //okuma başarılı olana kadar getID fonksiyonunu çağır
    successRead = getID(); 
     
  }
  while (!successRead);
  for ( int i = 0; i < mfrc522.uid.size; i++ )
  {
    //kartın UID'sini EEPROM'a kaydet
    EEPROM.write(i, readCard[i] ); 
   tam=tam+readCard[i];
    
  }
  digitalWrite(7, HIGH);
  delay(1000);
  digitalWrite(7, LOW);
  delay(1000);
  Serial.print(tam);
  tam="";
  //buzzer başlangıç

  /*
for (int i = 0; i < notaSayisi; i++)
  {
    tone(buzzerPin, notalar[i]);
    delay(500);
    noTone(buzzerPin);
    delay(20);
  }
  
*/


for (int Nota = 0; Nota <24; Nota++) 
    {
      int sure = tempo/notasuresi[Nota];
     tone(buzzerPin, melodi[Nota],sure);// 8 numarali bacaktan notalarin frekanslarini nota uzunluklarina göre gönder
     delay(sure*1.2);// notalar arasinda biraz beklesin. Eger 1 ile çarparsaniz notalar birbirine bitisik olur
    }

    noTone(buzzerPin);
  //buzzer bitiş

  //motor kod başla

  for(poz = 0; poz <= 180; poz += 1) // servomuzu 0dan 180 dereceye döndürüyoruz
  {
    servom.write(poz);              // servoya olması gereken pozisyon bilgisini gönderiyoruz
    delay(15);                       // her derecede beklemesi için
  }
 delay(1999);
  
  for(poz = 180; poz>=0; poz-=1)     // 180den 0a geri döndürüyoruz
  {
    servom.write(poz);              // servoya olması gereken pozisyon bilgisini gönderiyoruz
    delay(15);                       // her derecede beklemesi için
  }


  //motor kod bitiş



  

  
 // myservo.attach(6);
  //Serial.println("Kart EEPROM'a kaydedildi.");
  //Serial.println();
    
    }

 
  
}

void loop()
{




  

  
}

int getID() {
  //yeni bir kart okunmadıysa 0 döndür
  if ( ! mfrc522.PICC_IsNewCardPresent()) { 
    return 0;
  }


  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return 0;
  }
  //Serial.print("Kart UID'si: ");
  //kartın UID'sini byte byte oku ve seri monitöre yaz
  for (int i = 0; i < mfrc522.uid.size; i++) {  //
    readCard[i] = mfrc522.uid.uidByte[i];
    //Serial.print(readCard[i], HEX);
  }
  //Serial.println("");
  //kart okumayı durdur ve 1 döndür (okuma başarılı)
  mfrc522.PICC_HaltA();
  return 1;
}
