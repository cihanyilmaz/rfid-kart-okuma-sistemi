package com.iot.cihanylmz.iot;

import android.content.ClipData;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView saat;
    TextView giren;

    //List<String> arrList = new ArrayList<String>();
    private List<String> a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        a = new ArrayList<String>();
       //a.add("dsfdsf");
        saat=(TextView)findViewById(R.id.saat);
        giren=(TextView)findViewById(R.id.girenKisi);

        giren.setText("waiting");
        saat.setText("waiting");
//database bağlantımızı kuruyoruz
        DatabaseReference dbRef= FirebaseDatabase.getInstance().getReference().child("server").child("users").child("gelen");
        System.out.println("gelen:"+dbRef);
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
            User user =new User();

                user =dataSnapshot.getValue(User.class);
                    saat.setText(user.saati);
                System.out.println("gelen:"+user.adi);
                //arrList.add("adi: "+user.adi+"saat: "+user.saati);
               a.add("adi: "+user.adi+"  saat: "+user.saati);
                    giren.setText(user.adi);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }




        };


      //  DatabaseReference ddRef= FirebaseDatabase.getInstance().getReference().child("Kullanıcılar");
        dbRef.addValueEventListener(postListener);



    }


    public void allPerson(View view) {

        Intent newIntent=new Intent(getApplicationContext(),AllEnterPerson.class);
        Bundle bundle = new Bundle();

        newIntent.putStringArrayListExtra("arrList", (ArrayList<String>) a);

        startActivity(newIntent);

    }
}
